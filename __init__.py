# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import phyto
from . import product


def register():
    Pool.register(
        configuration.Configuration,
        product.RegistryProduct,
        product.Template,
        product.Product,
        phyto.Party,
        phyto.PartyPhytoStaff,
        module='phyto', type_='model')
