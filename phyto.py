# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Id


class PartyPhytoStaff(ModelSQL):
    """Party Phyto Staff"""
    __name__ = 'party.phyto_staff'

    party = fields.Many2One('party.party', 'Party', required=True)
    phyto_staff = fields.Many2One('party.party', 'Phyto Staff', required=True)


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    phyto_staff = fields.Many2Many('party.phyto_staff',
        'party', 'phyto_staff', 'Phyto Staff',
        domain=[('categories', 'child_of',
                Id('phyto', 'phyto_party_applier'), 'parent')])
    phyto_party = fields.Many2Many('party.phyto_staff',
        'phyto_staff', 'party', 'Party')
    phyto_number = fields.Char('Phytosanitary Number',
        help="Determines the number of the Official Register of Producers and"
        "Operators of phytosanitary products.")
    phyto_qualified = fields.Function(
        fields.Boolean('Qualified'),
        'get_phyto_applier_category')
    phyto_basic = fields.Function(
        fields.Boolean('Basic'),
        'get_phyto_applier_category')
    phyto_fumigator = fields.Function(
        fields.Boolean('Fumigator'),
        'get_phyto_applier_category')
    phyto_pilot = fields.Function(
        fields.Boolean('Pilot applier'),
        'get_phyto_applier_category')
    phyto_expiration_date = fields.Date('Phytosanitary expiration date')

    @staticmethod
    def _field_by_category():
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Category = pool.get('party.category')
        cualified_id = Modeldata.get_id('phyto', 'phyto_party_cualified')
        basic_id = Modeldata.get_id('phyto', 'phyto_party_basic')
        fumigator_id = Modeldata.get_id('phyto', 'phyto_party_fumigator')
        pilot_id = Modeldata.get_id('phyto', 'phyto_party_pilot')

        return {
            Category(cualified_id): 'phyto_qualified',
            Category(basic_id): 'phyto_basic',
            Category(fumigator_id): 'phyto_fumigator',
            Category(pilot_id): 'phyto_pilot'}

    @classmethod
    def get_phyto_applier_category(cls, records, names):
        fields_by_cat = cls._field_by_category()

        return {fname: {r.id: cat in r.categories for r in records}
            for cat, fname in fields_by_cat.items()}

    @fields.depends('categories')
    def on_change_categories(self):

        try:
            super().on_change_categories()
        except AttributeError:
            pass

        fields_by_cat = self._field_by_category()
        for cat, fname in fields_by_cat.items():
            setattr(self, fname, cat in self.categories)
