# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not
from trytond import backend
from trytond.transaction import Transaction


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    phyto_number = fields.Char('Phyto Number',
        states={'invisible': ~Eval('phyto')},
        depends=['phyto'])
    phyto = fields.Function(fields.Boolean('Phyto'),
        'get_phyto')
    common_name = fields.Char('Common name',
        states={
            'invisible': Not(Eval('phyto'))
        },
        depends=['phyto'])

    @classmethod
    def get_phyto(cls, records, name):
        pool = Pool()
        Category = pool.get('product.category')
        Modeldata = pool.get('ir.model.data')

        cat_ids = Category.search([
            ('parent', 'child_of', Modeldata.get_id('phyto',
                'phyto_product_phytosanitary'))])
        cat_ids = list(map(int, cat_ids))
        return {r.id: any(c.id in cat_ids for c in r.categories)
            for r in records}

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/form/notebook/page[@id="phyto"]', 'states', {
                    'invisible': Not(Eval('phyto')),
                    }),
            ]


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        Template = pool.get('product.template')

        table = cls.__table_handler__(cls, module_name)
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        template = Template.__table__()

        phyto_number_exists = table.column_exist('phyto_number')
        super().__register__(module_name)

        if phyto_number_exists and backend.name != 'sqlite':
            cursor.execute(*template.update(
                columns=[template.phyto_number],
                values=[sql_table.phyto_number],
                from_=[sql_table],
                where=(sql_table.template == template.id)))

            table.column_rename('phyto_number', 'phyto_number_old')


class RegistryProduct(ModelSQL, ModelView):
    '''Phytosanitary Registry Product'''
    __name__ = 'phyto.registry.product'

    code = fields.Char('Code', required=True, select=True)
    name = fields.Char('Name', required=True, translate=True, select=True)
    formulation = fields.Char('Formulation', required=True, select=True)
