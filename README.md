datalife_trytond-phyto
======================

The trytond-phyto module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-trytond-phyto/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-trytond-phyto)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
