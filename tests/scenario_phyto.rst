=====
Phyto
=====

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard


Install quality_control Module::

    >>> config = activate_modules('phyto')


Check applier cualification level fields::

    >>> Category = Model.get('party.category')
    >>> Party = Model.get('party.party')
    >>> cat_cualified, = Category.find([('name', '=', 'Cualified')], limit=1)
    >>> party = Party(name='Cualified')
    >>> party.categories.append(cat_cualified)
    >>> party.save()
    >>> party.phyto_qualified
    True
    >>> cat_basic, = Category.find([('name', '=', 'Basic')], limit=1)
    >>> party = Party(name='Basic')
    >>> party.categories.append(cat_basic)
    >>> party.save()
    >>> party.phyto_basic
    True
    >>> cat_pilot, = Category.find([('name', '=', 'Pilot Applier')], limit=1)
    >>> party = Party(name='Pilot')
    >>> party.categories.append(cat_pilot)
    >>> party.phyto_pilot
    True
    >>> party.save()
    >>> party.phyto_pilot
    True