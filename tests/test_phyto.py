# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import doctest_teardown, doctest_checker
from trytond.pool import Pool
from trytond.exceptions import UserError


class PhytoTestCase(ModuleTestCase):
    """Test Phyto module"""
    module = 'phyto'

    @with_transaction()
    def test_product(self):
        'Create product'
        pool = Pool()
        Uom = pool.get('product.uom')
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        Category = Pool().get('product.category')

        category_phyto, = Category.search([], limit=1)
        kg, = Uom.search([('name', '=', 'Kilogram')])
        g, = Uom.search([('name', '=', 'Gram')])
        template, = Template.create([{
            'name': 'Product phyto 1',
            'type': 'goods',
            'list_price': Decimal(1),
            'cost_price_method': 'fixed',
            'default_uom': kg.id,
            'categories': [('add', [category_phyto.id])]
        }])
        product, = Product.create([{
            'template': template.id,
            }])
        self.assert_(template.phyto)
        self.assert_(product.phyto)

    @with_transaction()
    def test_phyto_staff(self):
        'Create phyto party'
        Party = Pool().get('party.party')
        Category = Pool().get('party.category')

        category_phyto, = Category.search([], limit=1)
        phyto_staff1, = Party.create([{
            'name': 'Phyto Staff 1',
            'categories': [('add', [category_phyto.id])]
                    }])
        self.assert_(phyto_staff1.id)

    @with_transaction()
    def test_error_add_phyto_staff(self):
        'Test party append phyto staff to party'
        Party = Pool().get('party.party')

        phyto_staff1, = Party.create([{
            'name': 'Party 1',
        }])
        party1, = Party.search([], limit=1)

        self.assertRaises(UserError, Party.write, [party1], {
            'phyto_staff': [('add', [phyto_staff1.id])]})

    @with_transaction()
    def test_add_phyto_staff(self):
        'Test party append phyto staff to party'
        Party = Pool().get('party.party')
        Category = Pool().get('party.category')

        category_phyto, = Category.search([], limit=1)
        phyto_staff1, = Party.create([{
            'name': 'Phyto Staff 1',
            'categories': [('add', [category_phyto.id])]
                    }])
        party1, = Party.create([{
            'name': 'Phyto 1'
        }])

        Party.write([party1], {
            'phyto_staff': [('add', [phyto_staff1.id])]})


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PhytoTestCase))
    suite.addTests(doctest.DocFileSuite('scenario_phyto.rst',
           tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
           optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
